use json;
use rand;
use reqwest;
use std;
use uuid::Uuid;
/**
curl -i "http://127.0.0.1:2113/streams/newstream" \
    -H "Content-Type:application/vnd.eventstore.events+json" \
    -u "admin:changeit" \
    --data '[{"eventId":"fbf4a1a1-b4a3-4dfe-a01f-ec52c34e16e4","eventType":"event-type","data":{"a":"1"}}]'
*/
fn main() {
    let mut previous_id: [PreviousID; 4] = [
        PreviousID {
            tree: String::from(""),
            state: String::from(""),
            position: String::from(""),
        },
        PreviousID {
            tree: String::from(""),
            state: String::from(""),
            position: String::from(""),
        },
        PreviousID {
            tree: String::from(""),
            state: String::from(""),
            position: String::from(""),
        },
        PreviousID {
            tree: String::from(""),
            state: String::from(""),
            position: String::from(""),
        },
    ];
    loop {
        for i in 0..OPERATORS.len() {
            previous_id[i] = create_batch_and_send(OPERATORS[i], &previous_id[i]);
            println!("{:?}\n", previous_id);
        }
        std::thread::sleep(std::time::Duration::from_secs(5))
    }
}

fn send_an_event(event: json::JsonValue, stream: &str, user: &str) {
    let client = reqwest::blocking::Client::new();

    let addr = format!(
        "{server}/streams/{stream}-{user}",
        server = SERVER_ADDR,
        stream = stream,
        user = user,
    );

    let _ = client
        .post(addr.as_str())
        .header("Content-Type", CONTENT_TYPE)
        .basic_auth(USER, Some(PASSWORD))
        .body(event.dump())
        .send();

    //println!("{:#?}", res.unwrap());
}

fn create_batch_and_send(user: &str, previous_id: &PreviousID) -> PreviousID {
    let event_loc = create_a_localisation_event(user, &previous_id.position);
    send_an_event(event_loc.clone(), STREAM_POSITION, user);
    let event_state = create_a_state_event(user, &previous_id.state);
    send_an_event(event_state.clone(), STREAM_STATE, user);
    let event_tree = create_a_tree_picking_event(user, &previous_id.tree);
    send_an_event(event_tree.clone(), STREAM_TREE, user);
    PreviousID {
        state: event_state[0]["eventId"].to_string(),
        tree: event_tree[0]["eventId"].to_string(),
        position: event_loc[0]["eventId"].to_string(),
    }
}
fn create_a_localisation_event(user: &str, previous_id: &String) -> json::JsonValue {
    let uuid = Uuid::new_v4().to_string();
    let event_type = if rand::random() || *previous_id == String::from("") {
        AQ_EVENT
    } else {
        CORRECTION_EVENT
    };
    let (long, lat) = (rand::random::<f64>(), rand::random::<f64>());
    let mut event = json::array![json::object! {
        "eventId":uuid.clone(),
        "eventType":event_type,
        "data":{
            "long":long,
            "lat":lat,
        },
        "metadata":{
            "$correlationId":user,
        }
    }];
    if event_type == CORRECTION_EVENT {
        let _ = event[0]["metadata"].insert("corretionOf", previous_id.clone());
        event
    } else {
        event
    }
}

fn create_a_tree_picking_event(user: &str, previous_id: &String) -> json::JsonValue {
    let uuid = Uuid::new_v4().to_string();
    let event_type = if rand::random() || *previous_id == String::from("") {
        AQ_EVENT
    } else {
        CORRECTION_EVENT
    };
    let tree = Uuid::new_v4().to_string();
    let mut event = json::array![json::object! {
        "eventId":uuid.clone(),
        "eventType":event_type,
        "data":{
            "tree":tree.clone(),
        },
        "metadata":{
            "$correlationId":user,
        }
    }];
    if event_type == CORRECTION_EVENT {
        let _ = event[0]["metadata"].insert("corretionOf", previous_id.clone());
        event
    } else {
        event
    }
}

fn create_a_state_event(user: &str, previous_id: &String) -> json::JsonValue {
    let uuid = Uuid::new_v4().to_string();
    let event_type = if rand::random() || *previous_id == String::from("") {
        AQ_EVENT
    } else {
        CORRECTION_EVENT
    };
    let state = if rand::random() {
        "moving"
    } else {
        "tree picking"
    };
    let mut event = json::array![json::object! {
        "eventId":uuid.clone(),
        "eventType":event_type,
        "data":{
            "state":state,
        },
        "metadata":{
            "$correlationId":user,
        }
    }];
    if event_type == CORRECTION_EVENT {
        let _ = event[0]["metadata"].insert("corretionOf", previous_id.clone());
        event
    } else {
        event
    }
}

const SERVER_ADDR: &str = "http://localhost:2113";
const CONTENT_TYPE: &str = "application/vnd.eventstore.events+json";
const USER: &str = "admin";
const PASSWORD: &str = "changeit";

const AQ_EVENT: &str = "AQ";
const CORRECTION_EVENT: &str = "correction";

const OPERATOR_1_N: &str = "1";
const OPERATOR_2_N: &str = "2";
const OPERATOR_1_S: &str = "3";
const OPERATOR_2_S: &str = "4";
const OPERATORS: [&str; 4] = [OPERATOR_1_N, OPERATOR_2_N, OPERATOR_1_S, OPERATOR_2_S];

const STREAM_POSITION: &str = "pos";
const STREAM_TREE: &str = "tree";
const STREAM_STATE: &str = "state";

#[derive(Debug)]
struct PreviousID {
    pub tree: String,
    pub position: String,
    pub state: String,
}
